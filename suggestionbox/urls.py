from django.conf.urls import patterns, include, url
from suggestionbox import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^detail/(?P<suggestion_id>\d+)/$', views.detail, name='detail'),
    url(r'^category/(?P<category_id>\d+)/$', views.category, name='category'),
    url(r'^status/(?P<status_id>\d+)/$', views.status, name='status'),
    url(r'^detail/(?P<suggestion_id>\d+)/add_comment/$', views.add_comment, name='add_comment'),
    url(r'^detail/(?P<suggestion_id>\d+)/edit_status/$', views.edit_status, name='edit_status'),
    url(r'^add_suggestion/$', views.add_suggestion, name='add_suggestion'),
)
