# Create your views here.
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.core.mail import send_mail

from suggestionbox.models import Suggestion, Comment, Status, Category
from suggestionbox.forms import AddComment, AddSuggestion, EditStatus
# view for our index
@login_required
def index(request):
    suggestions = Suggestion.objects.all().order_by('-pub_date')
    categories = Category.objects.all()
    statuses = Status.objects.all()
    paginator = Paginator(suggestions, 5)
    page = request.GET.get('page')
    
    try:
        suggestions= paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        suggestions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        suggestions = paginator.page(paginator.num_pages)
    user = request.user
    context = {
        'suggestions': suggestions,
	'user': user,
	'categories': categories,
	'statuses': statuses,
    }

    return render(request, 'suggestionbox/index.html', context)

@login_required
def detail(request, suggestion_id):
    suggestion = get_object_or_404(Suggestion, pk=suggestion_id)
    suggestions = Suggestion.objects.all().order_by('-pub_date')#for the navbar
    categories = Category.objects.all()
    statuses = Status.objects.all()
    comments = Comment.objects.all().filter(sid=suggestion_id)
    user = request.user
    context = {
        'suggestion': suggestion,
	'suggestions': suggestions,
	'comments': comments,
	'user': user,
        'categories': categories,
	'statuses': statuses,
    }

    return render(request, 'suggestionbox/detail.html', context)
@login_required
def add_comment(request, suggestion_id):
	suggestion = get_object_or_404(Suggestion, pk=suggestion_id)
	suggestions = Suggestion.objects.all().order_by('-pub_date')#for the navbar
        categories = Category.objects.all()
        statuses = Status.objects.all()
	comment = Comment(user=request.user, sid=suggestion)
	form = AddComment(request.POST or None, instance=comment)

	if form.is_valid():
		form.save()
		messages.success(request, 'Successfully added a comment.')
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
	context = {
        'suggestion': suggestion,
	'suggestions': suggestions,
	'form': form,
	'categories': categories,
	'statuses': statuses,
	}
	
	return render(request, 'suggestionbox/add_comment.html', context)
@login_required
def add_suggestion(request):
	default_status=Status(pk=1)#the default pending status
	suggestion = Suggestion(user=request.user, status=default_status)
	suggestions = Suggestion.objects.all().order_by('-pub_date')#for the navbar
        categories = Category.objects.all()
	statuses = Status.objects.all()

	form = AddSuggestion(request.POST or None, instance=suggestion)

	if form.is_valid():
		form.save()
		messages.success(request, 'Successfully added a suggestion.')
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
	context = {
	'suggestions': suggestions,
	'form': form,
	'categories': categories,
	'statuses': statuses,
	}
	
	return render(request, 'suggestionbox/add_suggestion.html', context)
@login_required
def edit_status(request, suggestion_id):
	suggestion = get_object_or_404(Suggestion, pk=suggestion_id)
	suggestions = Suggestion.objects.all().order_by('-pub_date')#for the navbar
        categories = Category.objects.all()
	statuses = Status.objects.all()
	form = EditStatus(request.POST or None, instance=suggestion)
	if request.user.id ==1: #we only submit the form if the user is the admin.
		if form.is_valid():
			form.save()
			messages.success(request, 'Status successfully modified.')
			#next line will send an email, that will shou up in the console, not actually to an email address...
			#suggestion.user.email_user('A suggestion you have mades status has been modified', 's status has been modified to ', 'sgoeddel247@gmail.com') 
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
	context = {
	'suggestions': suggestions,
	'suggestion': suggestion,
	'form': form,
	'categories': categories,
	'statuses': statuses,
	
	}
	
	return render(request, 'suggestionbox/edit_status.html', context)
@login_required
def category(request, category_id):
    suggestions = Suggestion.objects.all().filter(category = category_id).order_by('-pub_date')
    categories = Category.objects.all()
    statuses = Status.objects.all()
    cat = get_object_or_404(Category, pk=category_id)
    paginator = Paginator(suggestions, 5)
    page = request.GET.get('page')
    
    try:
        suggestions= paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        suggestions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        suggestions = paginator.page(paginator.num_pages)

    user = request.user
    context = {
        'suggestions': suggestions,
	'user': user,
	'cat': cat,
	'categories': categories,
	'statuses': statuses,
    }

    return render(request, 'suggestionbox/category.html', context)

def status(request, status_id):
    suggestions = Suggestion.objects.all().filter(status = status_id).order_by('-pub_date')
    categories = Category.objects.all()
    statuses = Status.objects.all()
    status = get_object_or_404(Status, pk=status_id)
    paginator = Paginator(suggestions, 5)
    page = request.GET.get('page')
    
    try:
        suggestions= paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        suggestions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        suggestions = paginator.page(paginator.num_pages)
    user = request.user
    context = {
        'suggestions': suggestions,
	'user': user,
	'status': status,
	'categories': categories,
	'statuses': statuses,
    }

    return render(request, 'suggestionbox/status.html', context)






