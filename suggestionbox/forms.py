from django import forms
from django.forms import ModelForm
from suggestionbox.models import Comment, Suggestion, Status

class AddComment(ModelForm):
	class Meta:
		model = Comment
		widgets = {
          		'comment': forms.Textarea(attrs={'rows':3, 'cols':15}),
        	}
		fields=['comment']

class AddSuggestion(ModelForm):
	class Meta:
		model = Suggestion
		widgets = {
          		'description': forms.Textarea(attrs={'rows':3, 'cols':15}),
        	}
		fields=['title','description','category']

class EditStatus(ModelForm):
	class Meta:
		model= Suggestion
		fields=['status']
