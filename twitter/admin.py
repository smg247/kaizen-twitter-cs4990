from django.contrib import admin

from twitter.models import Profile, Post

class ProfileAdmin(admin.ModelAdmin):
    search_fields = ['user', 'bio']
    list_display = ('user', 'bio',)



class PostAdmin(admin.ModelAdmin):
    search_fields = ['profile', 'body']
    list_display = ('profile', 'body', 'pub_date','linked_profile',)


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Post, PostAdmin)
