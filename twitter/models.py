from django.db import models
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

class Profile(models.Model):
	user = models.ForeignKey(User)
	bio = models.TextField()
	image = models.ImageField(upload_to='images/%Y/%m/%d/')
	following = models.ManyToManyField('self', symmetrical=False)
	
	def __unicode__(self):
		return self.user.username

class Post(models.Model):
	profile = models.ForeignKey(Profile)
	body = models.CharField(max_length=140)
	pub_date = models.DateTimeField(auto_now_add=True)
	linked_profile = models.ForeignKey(Profile, related_name='linked_profile', null=True)

	def __unicode__(self):
		return self.body






##
##	def linked_bool(self):
##		if '@' in self.body:
##			return True
##		else:
##			return False
##
##	def linked_user(self):
##		if '@' in self.body:
##			start = self.body.find('@')
##			end = self.body.find(' ', start)
##			return self.body[start:end]
##
##	def linked(self):
##		if '@' in self.body:
##			start = self.body.find('@')
##			end = self.body.find(' ', start)
##			profile = get_object_or_404(Profile, user=self.body[start+1:end])
##			return profile.id
##		else:
##			return False
	


