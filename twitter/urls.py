from django.conf.urls import patterns, include, url
from twitter import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^/profile/(?P<profile_id>\d+)/$', views.profile, name='profile'),
    url(r'^/profile/follow/(?P<profile_id>\d+)/(?P<fol>\d+)/$', views.follow, name='follow'),
    url(r'^/profile/edit/(?P<profile_id>\d+)/$', views.edit_profile, name='edit_profile'),
    url(r'^/profile/add/$', views.add_profile, name='add_profile'),
    url(r'^/add_post/$', views.add_post, name='add_post'),
    url(r'^/delete_post/(?P<post_id>\d+)/$', views.delete_post, name='delete_post'),
)
