# Create your views here.
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django import forms

from twitter.models import Profile, Post
from twitter.forms import EditProfile, AddPost, AddProfile

@login_required
def index(request):
	user = request.user
	profiles = Profile.objects.all().order_by('-id')
	
	new = True
	for profile in profiles: #I'm not certain this is the best way to do this, determining if the user has a profile
		if int(profile.id) == int(user.id):
			new = False
			break
	if not new:
		user_profile = Profile.objects.get(pk=user.id)
		context = {
		'profiles': profiles,
		'user':user,
		'new':new,
		'user_profile':user_profile,
		}
	else:
		context = {
		'profiles': profiles,
		'user':user,
		'new':new,
		}
	

	return render(request, 'twitter/index.html', context)

@login_required
def profile(request, profile_id):
	user = request.user
	profile = get_object_or_404(Profile, pk=profile_id)
	user_profile = get_object_or_404(Profile, pk=user.id)
	posts = Post.objects.all().filter(profile = profile_id).order_by('-pub_date')
	all_posts = Post.objects.filter(profile__in = profile.following.all()).order_by('-pub_date')
	mentions = Post.objects.all().filter(linked_profile = profile.id).order_by('-pub_date')
	new =False #this is in here so the navbar works
	cur_user = False

	if int(user.id) == int(profile_id): #if the current user is the same as the profile
		cur_user = True

	following = False
	for user_profile in request.user.profile_set.all():
        	if profile in user_profile.following.all():
            		following = True
            		break

	context = {
        'profile': profile,
	'user':user,
	'user_profile':user_profile,
	'posts':posts,
	'cur_user':cur_user,
	'all_posts':all_posts,
	'following':following,
	'new':new,
	'mentions':mentions,
        }

	return render(request, 'twitter/profile.html', context)

@login_required
def edit_profile(request, profile_id):
	user = request.user
	profile = get_object_or_404(Profile, pk=profile_id)
	form = EditProfile(request.POST or None, request.FILES or None, instance=profile)
	cur_user = False
	new =False #this is in here so the navbar works
	if int(user.id) == int(profile_id): #if the current user is the same as the profile
		cur_user = True

	if cur_user: #only show the form if the user owns this profile
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('twitter:profile', args=(profile.id,)))
	context = {
        'profile': profile,
	'user':user,
	'form':form,
	'new':new,
        }
	return render(request, 'twitter/edit_profile.html', context)

@login_required
def add_post(request):
	user = request.user
	profile = get_object_or_404(Profile, pk=user.id)
	
	new =False #this is in here so the navbar works

	#AddPost.base_fields['linked_profile'] = forms.ModelChoiceField(queryset=Profile.objects.filter(profile__in = profile.following.all()))
	form = AddPost(request.POST or None)
	if form.is_valid():
		cd = form.cleaned_data
		if cd.get('linked_profile'):
			linked_pro = Profile.objects.get(pk=cd.get('linked_profile'))
		else:
			linked_pro = None
		new_post = Post.objects.create(body = cd.get('body'), linked_profile = linked_pro, profile = profile )
		new_post.save()
		return HttpResponseRedirect(reverse('twitter:profile', args=(profile.id,)))
	context = {
	'user':user,
	'form':form,
	'new':new,
        }
	return render(request, 'twitter/add_post.html', context)

def add_profile(request):
	user = request.user
	profile, created = Profile.objects.get_or_create(pk=user.id,user=user)
	new =False #this is in here so the navbar works
	form = AddProfile(request.POST or None, request.FILES or None, instance=profile)

	if form.is_valid():
		profile.following.add(profile) #makes it so the new profile follows itself, which is neccessary to correctly display posts
		form.save()
		return HttpResponseRedirect(reverse('twitter:profile', args=(user.id,)))
	context = {
        'profile': profile,
	'user':user,
	'form':form,
	'new':new,
        }
	return render(request, 'twitter/add_profile.html', context)

def follow(request, profile_id, fol):
	user = request.user
	profile = get_object_or_404(Profile, pk=profile_id)
	user_profile = get_object_or_404(Profile, pk=user.id)
	if int(fol) == 1: #if we are trying to follow
		user_profile.following.add(profile)
		return HttpResponseRedirect(reverse('twitter:profile', args=(profile.id,)))
	else: #if we are trying to unfollow
		user_profile.following.remove(profile)
		return HttpResponseRedirect(reverse('twitter:index'))

def delete_post(request, post_id):
	user = request.user
	post = get_object_or_404(Post, pk=post_id)
	post.delete()
	return HttpResponseRedirect(reverse('twitter:profile', args=(user.id,)))

