from django import forms
from django.forms import ModelForm
from django import forms
from twitter.models import Profile, Post

class EditProfile(ModelForm):
	class Meta:
		model = Profile
		widgets = {
          		'bio': forms.Textarea(attrs={'rows':3, 'cols':15}),
        	}
		fields=['bio','image']

class AddPost(forms.Form):
	body = forms.CharField(max_length=140)
	linked_profile = forms.ModelMultipleChoiceField(queryset=Profile.objects.all(), required=False)
	
	def __init__(self, *args, **kwargs): #changing the lable of linked_profile, as it is not clear what that means to the user
		super(AddPost, self).__init__(*args, **kwargs)
		self.fields['linked_profile'].label = "Tag a user (optional)"


class AddProfile(ModelForm):
	class Meta:
		model = Profile
		widgets = {
          		'bio': forms.Textarea(attrs={'rows':3, 'cols':15}),
        	}
		fields=['bio','image']

	
